#!/bin/bash

# SPI 0
config-pin p9.17 spi_cs
config-pin p9.21 spi
config-pin p9.18 spi
config-pin p9.22 spi_sclk

# GPIO
## IO UPDATE
config-pin p9.15 gpio
## RESET BUFF
config-pin p9.16 gpio

# SPI 1 (!! Need to disable HDMI interface !!)
#config-pin p9.20 spi_cs
#config-pin p9.28 spi_cs
#config-pin p9.19 spi_cs
#config-pin p9.42 spi_cs
#config-pin p9.29 spi
#config-pin p9.30 spi
#config-pin p9.31 spi_sclk

exit 0
